# Inversion of Control

Inversion of control is a well known design principle for making loosely coupled code. An important subset of IoC
is about managing dependencies. Generally you would want to separate the code that instantiates objects from the
code that uses these objects. This makes it very easy to replace an implementation of an object for another
implementation when needed, for instance when the other implementations has better performance or when a new
device needs to be managed.

For example, a component handles a request according to some business rules:

- a request is received by the component and transformed into a domain object,
- the request needs to be logged,
- the request is processed according to one or more business rules and results in a response,  
- the response is logged before being returned to the caller,
- the response is returned to the caller.

Within this component three other components may be needed for:

- logging the request,
- processing the request into a response,
- logging the response.

When applying inversion of control, these three components would be instantiated outside the component
that uses them. However, the component that uses them applies a certain flow to the data, 
i.e. logging the request, processing, and
logging the response. These are the business rules and the flow is still coded inside the component.
The business rules are still coupled to the component, when the business rules change the component 
has to be modified. 

_**Gumo allows to decouple the business rules from components that use them**_. 
Business rules are, like instantiated
components, created elsewhere and can be supplied to components. When business rules are changed the
components that use them need not be modified. Components only need to be modified when the interface
to the business rules changes, or when the interface to the outside world (the request and/or the response)
changes.

----

[See the wiki for more information](https://bitbucket.org/pukanito/gumo/wiki/)

For downloading Gumo see the [mvnrepository](http://mvnrepository.com/artifact/org.bitbucket.pukanito/gumo)

[![Javadocs](http://javadoc.io/badge/org.bitbucket.pukanito/gumo.svg)](http://javadoc.io/doc/org.bitbucket.pukanito/gumo)
