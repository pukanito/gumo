/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.function.Consumer;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

public class GumoModuleTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private static class StrToIntFunction implements Function<String, Integer> {
        @Override
        public Integer apply(String s) { return 0; }
    }

    private static class StringConsumer implements Consumer<String> {
        @Override
        public void accept(String s) { }
    }

    private static class IntegerConsumer implements Consumer<Integer> {
        @Override
        public void accept(Integer s) { }
    }

    @Test
    public void testBindingAlreadyBoundTypeShouldThrowIllegalStateException() {
        exception.expect(IllegalStateException.class);
        exception.expectMessage("already contains a binding for: Key[type=java.lang.String, annotation=[none]]");
        new GumoModule() {
            @Override
            protected void configure() {
                bind(String.class).toConsumer(StringConsumer.class);
                this.<String, Integer>bind(String.class).toFunction(StrToIntFunction.class);
            }
        };
    }

    @Test
    public void testGetBindingsShouldReturnAllApplicableBindings() {
        final GumoModule module = new GumoModule() {
            @Override
            protected void configure() {
                bind(String.class).toConsumer(StringConsumer.class);
                bind(Integer.class).toConsumer(IntegerConsumer.class);
                bind(String.class, "B1").toConsumer(StringConsumer.class);
                bind(Integer.class, "B1").toConsumer(IntegerConsumer.class);
                bind(String.class, "B2").toConsumer(StringConsumer.class);
                this.<String, Integer>bind(String.class, "B3").toFunction(StrToIntFunction.class);
                this.<String, Integer>bind(String.class, "B4").toFunction(StrToIntFunction.class);
            }
        };
        assertThat(module.getBindings("A".getClass()), hasSize(1));
        assertThat(module.getBindings("B".getClass(), "B1", "B3"), hasSize(3));
        assertThat(module.getBindings("C".getClass(), "B1", "B3", "B2"), hasSize(4));
        assertThat(module.getBindings(Integer.valueOf(0).getClass(), "B1", "B3", "B2"), hasSize(2));
    }

    @Test
    public void testGetBindingsShouldReturnAllApplicableBindingsFromAllParents() {
        final GumoModule module = new GumoModule() {
            @Override
            protected void configure() {
                bind(String.class).toConsumer(StringConsumer.class);
                bind(String.class, "B1").toConsumer(StringConsumer.class);
            }
        };
        final GumoModule childModule = new GumoModule(module) {
            @Override
            protected void configure() {
                this.<String, Integer>bind(String.class).toFunction(StrToIntFunction.class);
            }
        };
        assertThat(childModule.getBindings("A".getClass()), hasSize(2));
        assertThat(childModule.getBindings("A".getClass(), "B1"), hasSize(3));
    }
}
