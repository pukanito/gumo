/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.function.Consumer;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;

public class GumoBindingTest {

    @Rule
    public final ExpectedException exception = ExpectedException.none();

    private static class StrToIntFunction implements Function<String, Integer> {
        @Override
        public Integer apply(String s) { return 0; }
    }

    private static class StringConsumer implements Consumer<String> {
        @Override
        public void accept(String s) { }
    }

    @Test
    public void testBuilderContainingConsumerShouldHaveNullConsumerWhenLaterAddingFunction() {
        final GumoBinding.Builder<String, Integer> builder = new GumoBinding.Builder<>();
        builder.toConsumer(StringConsumer.class);
        builder.toFunction(StrToIntFunction.class);
        assertThat(builder.build().getDestinationConsumerType(), nullValue());
        assertThat(builder.build().getDestinationFunctionType(), notNullValue());
    }

    @Test
    public void testBuilderContainingFunctionShouldHaveNullFunctionWhenLaterAddingConsumer() {
        final GumoBinding.Builder<String, Integer> builder = new GumoBinding.Builder<>();
        builder.toFunction(StrToIntFunction.class);
        builder.toConsumer(StringConsumer.class);
        assertThat(builder.build().getDestinationFunctionType(), nullValue());
        assertThat(builder.build().getDestinationConsumerType(), notNullValue());
    }
}
