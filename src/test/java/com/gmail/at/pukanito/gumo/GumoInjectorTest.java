/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsSame.sameInstance;

public class GumoInjectorTest {

    @Test
    public void testGumoInjectorShouldContainTheProcessor() {
        final AbstractModule module = new AbstractModule() {
            @Override
            protected void configure() { }
        };
        final Injector guiceInjector = Guice.createInjector(module);
        final GumoModule config = new GumoModule() {
            @Override
            protected void configure() { }
        };
        final Processor processor = new Processor(config, guiceInjector);
        final Injector gumoInjector = Gumo.createInjector(guiceInjector, processor);
        assertThat(gumoInjector.getInstance(Processor.class), sameInstance(processor));
    }
}
