/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumotest;

import com.gmail.at.pukanito.gumo.Gumo;
import com.gmail.at.pukanito.gumo.GumoModule;
import com.gmail.at.pukanito.gumo.Processor;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Singleton;
import org.junit.Test;

import java.util.function.Consumer;
import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Simple Function and Consumer test.
 *
 * Test Gumo in a different package to check access of API methods.
 */
public class GumoTest {

    private static class TestModule extends AbstractModule {
        @Override
        protected void configure() {
            bind(DoubleConsumer.class).in(Singleton.class);
        }
    }

    private static class TestGumoModule extends GumoModule {
        @Override
        protected void configure() {
            bind(Double.class).toConsumer(DoubleConsumer.class);
            bind(Double.class, "F1").toConsumer(DoubleConsumer.class);
            bind(Double.class, "F2").toConsumer(DoubleConsumer.class);
            this.<Double, Integer>bind(Double.class, "F3").toFunction(DblToIntFunction2.class);
        }
    }

    private static class TestGumoChildModule extends GumoModule {
        TestGumoChildModule(final GumoModule parentModule) {
            super(parentModule);
        }
        @Override
        protected void configure() {
            this.<String, Integer>bind(String.class).toFunction(StrToIntFunction.class);
            this.<Double, Integer>bind(Double.class).toFunction(DblToIntFunction.class);
        }
    }

    private static class StrToIntFunction implements Function<String, Integer> {
        @Override
        public Integer apply(String s) {
            return Integer.valueOf(s);
        }
    }

    private static class DblToIntFunction implements Function<Double, Integer> {
        @Override
        public Integer apply(Double d) {
            return d.intValue() * 2;
        }
    }

    private static class DblToIntFunction2 implements Function<Double, Integer> {
        @Override
        public Integer apply(Double d) {
            return d.intValue() * 3;
        }
    }

    private static class DoubleConsumer implements Consumer<Double> {
        double result = 0.0;
        @Override
        public void accept(Double aDouble) {
            result += aDouble;
        }
    }

    @Test
    public void testSimpleGumoProcessorConsumerShouldConsumeCorrectly() {
        final TestModule module = new TestModule();
        final Injector guiceInjector = Guice.createInjector(module);
        final GumoModule config = new TestGumoModule();
        final Processor processor = new Processor(config, guiceInjector);
        final Injector gumoInjector = Gumo.createInjector(guiceInjector, processor);

        gumoInjector.getInstance(DoubleConsumer.class).result = 0.0;
        gumoInjector.getInstance(Processor.class).apply(2.0);
        assertThat(gumoInjector.getInstance(DoubleConsumer.class).result, equalTo(2.0));
        gumoInjector.getInstance(DoubleConsumer.class).result = 0.0;
        gumoInjector.getInstance(Processor.class).apply(3.0, "F1", "F2");
        assertThat(gumoInjector.getInstance(DoubleConsumer.class).result, equalTo(9.0));
    }

    @Test
    public void testSimpleGumoProcessorFunctionWithParentProcessorShouldFunctionCorrectly() {
        final TestModule module = new TestModule();
        final Injector guiceInjector = Guice.createInjector(module);
        final GumoModule gumoModule = new TestGumoModule();
        final GumoModule gumoChildModule = new TestGumoChildModule(gumoModule);
        final Processor childProcessor = new Processor(gumoChildModule, guiceInjector);
        final Injector gumoInjector = Gumo.createInjector(guiceInjector, childProcessor);

        final Integer resultaat = gumoInjector.getInstance(Processor.class).apply("15", "UNUSED");
        assertThat(resultaat, equalTo(15));
        gumoInjector.getInstance(DoubleConsumer.class).result = 0.0;
        final Integer resultaat2 = gumoInjector.getInstance(Processor.class).apply(2.0, "F1", "F3");
        assertThat(resultaat2, equalTo(4));
        assertThat(gumoInjector.getInstance(DoubleConsumer.class).result, equalTo(4.0));
    }
}
