/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumotest;

import com.gmail.at.pukanito.gumo.Gumo;
import com.gmail.at.pukanito.gumo.GumoModule;
import com.gmail.at.pukanito.gumo.Processor;
import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Test;

import java.util.function.Function;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

/**
 * Simple recursive Function test.
 *
 * Test Gumo in a different package to check access of API methods.
 */
public class GumoRecursionTest {

    private static class TestModule extends AbstractModule {
        @Override
        protected void configure() { }
    }

    private static class TestGumoModule extends GumoModule {
        @Override
        protected void configure() {
            this.<String, Integer>bind(String.class).toFunction(StrToIntFunction.class);
            this.<Integer, Double>bind(Integer.class).toFunction(IntToDblFunction.class);
        }
    }

    private static class StrToIntFunction implements Function<String, Integer> {
        @Override
        public Integer apply(String s) {
            return Integer.valueOf(s);
        }
    }

    private static class IntToDblFunction implements Function<Integer, Double> {
        @Override
        public Double apply(Integer n) {
            return n + 1.0;
        }
    }


    @Test
    public void testSimpleGumoProcessorConsumerShouldConsumeCorrectly() {
        final TestModule module = new TestModule();
        final Injector guiceInjector = Guice.createInjector(module);
        final GumoModule config = new TestGumoModule();
        final Processor processor = new Processor(config, guiceInjector);
        final Injector gumoInjector = Gumo.createInjector(guiceInjector, processor);

        final Double resultaat = gumoInjector.getInstance(Processor.class).apply("15");
        assertThat(resultaat, equalTo(16.0));
    }
}
