/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import com.google.inject.Injector;

/**
 * Main class of Gumo.
 *
 * It creates an instance of an {@link Injector} that contains a configured Processor.
 */
public class Gumo {

    /**
     * Create a Guice injector that also contains a {@link Processor}.
     *
     * @param guiceInjector Guice Injector that will act as a parent injector of the created injector.
     * @param processor a Gumo Processor.
     * @return a Guice Injector that also contains the specified {@link Processor}.
     */
    public static Injector createInjector(final Injector guiceInjector, final Processor processor) {
        return new GumoInjector(guiceInjector, processor);
    }
}
