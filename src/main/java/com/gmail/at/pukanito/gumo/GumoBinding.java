/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Helper class for binding from a source type to a destination type Consumer or Function.
 *
 * @param <T> the source type.
 * @param <R> the destination type (only applicable for functions).
 */
public class GumoBinding<T, R> {

    private final Class<? extends Consumer<T>> destinationConsumerType;
    private final Class<? extends Function<T, R>> destinationFunctionType;

    private GumoBinding(final Builder<T, R> builder) {
        destinationConsumerType = builder.destinationConsumerType;
        destinationFunctionType = builder.destinationFunctionType;

    }

    Class<? extends Consumer<T>> getDestinationConsumerType() {
        return destinationConsumerType;
    }

    Class<? extends Function<T, R>> getDestinationFunctionType() {
        return destinationFunctionType;
    }

    /**
     * Binding builder that accepts the destination of a binding.
     *
     * @param <T> the source type.
     * @param <R> the destination type.
     */
    public static class Builder<T, R> {
        private Class<? extends Consumer<T>> destinationConsumerType;
        private Class<? extends Function<T, R>> destinationFunctionType;

        /**
         * Bind a consumer type.
         *
         * @param type type of the consumer.
         * @param <U> type of the consumer.
         */
        public <U extends Consumer<T>> void toConsumer(final Class<U> type) {
            destinationFunctionType = null;
            destinationConsumerType = type;
        }

        /**
         * Bind a function type.
         *
         * @param type type of the function.
         * @param <U> type of the function.
         */
        public <U extends Function<T, R>> void toFunction(final Class<U> type) {
            destinationConsumerType = null;
            destinationFunctionType = type;
        }

        GumoBinding<T, R> build() {
            return new GumoBinding<>(this);
        }
    }
}
