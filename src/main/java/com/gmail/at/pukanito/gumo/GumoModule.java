/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import com.google.inject.Key;
import com.google.inject.name.Names;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Configuration for a {@link Processor}.
 */
public abstract class GumoModule {

    private final Map<Key<?>, GumoBinding.Builder> bindingBuildersMap = new HashMap<>();
    private final Map<Key<?>, GumoBinding<?, ?>> bindingsMap;
    private final GumoModule parentModule;

    /**
     * Create a new Gumo module, configured according to the configure() method.
     */
    protected GumoModule() {
        this(null);
    }

    /**
     * Create a new Gumo module, configured according to the configure() method, with
     * the specified module as parent module.
     *
     * @param parentModule a parent module.
     */
    protected GumoModule(final GumoModule parentModule) {
        this.parentModule = parentModule;
        configure();
        bindingsMap = bindingBuildersMap.entrySet().stream()
                .collect(Collectors.<Map.Entry<Key<?>, GumoBinding.Builder>, Key<?>, GumoBinding<?, ?>>toMap(Map.Entry::getKey, o -> o.getValue().build()));
    }

    /**
     * Configure this GumoModule by specifying all bindings.
     */
    abstract protected void configure();

    /**
     * Binder for a default class type.
     *
     * @param type the source type for the binding.
     * @param <T> the source type for the binding.
     * @param <R> the destination type for the binding.
     * @return a binding builder that can bind to a Consumer or Function.
     */
    protected <T, R> GumoBinding.Builder<T, R> bind(final Class<T> type) {
        return bind(Key.get(type));
    }

    /**
     * Binder for a named class type.
     *
     * @param type the source type for the binding.
     * @param <T> the source type for the binding.
     * @param <R> the destination type for the binding.
     * @return a binding builder that can bind to a Consumer or Function.
     */
    protected <T, R> GumoBinding.Builder<T, R> bind(final Class<T> type, final String annotationName) {
        return bind(Key.get(type, Names.named(annotationName)));
    }

    private <T, R> GumoBinding.Builder<T, R> bind(final Key<T> key) {
        if (bindingBuildersMap.containsKey(key))
            throw new IllegalStateException("Module already contains a binding for: " + key);
        final GumoBinding.Builder<T, R> builder = new GumoBinding.Builder<>();
        bindingBuildersMap.put(key, builder);
        return builder;
    }

    /**
     * @param inClass class type for which to get bindings.
     * @param annotationNames annotation names for which to get bindings (default without name is always included).
     * @return all applicable bindings of this module and parent processes modules.
     */
    List<GumoBinding> getBindings(final Class<?> inClass, final String... annotationNames) {
        return getModules(this)
                .map(m -> m.getModuleBindings(inClass, annotationNames))
                .flatMap(x -> x)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    /**
     * @param module the module to start with.
     * @return a stream of modules, starting with this module followed by all modules of parent processors if present.
     */
    private Stream<GumoModule> getModules(final GumoModule module) {
        return module.parentModule == null ? Stream.of(module) : Stream.concat(Stream.of(module), getModules(parentModule));
    }

    /**
     * @param inClass the class type.
     * @param annotationNames the binding names.
     * @return a stream of Gumo bindings of this module for the specified class type and binding names. Bindings are
     *          ordered: first bindings with annotation names in the order given, then the default binding. The stream
     *          may contain nulls.
     */
    private Stream<GumoBinding<?, ?>> getModuleBindings(final Class<?> inClass, final String... annotationNames) {
        final Stream<GumoBinding<?, ?>> annotatedBindings = Arrays.stream(annotationNames).map(a -> bindingsMap.get(Key.get(inClass, Names.named(a))));
        final Stream<GumoBinding<?, ?>> defaultBinding = Stream.of(bindingsMap.get(Key.get(inClass)));
        return Stream.concat(annotatedBindings, defaultBinding);
    }
}
