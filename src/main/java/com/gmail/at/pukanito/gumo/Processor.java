/*
 * Copyright (C) 2018 Pukanito Forager.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * https://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.gmail.at.pukanito.gumo;

import com.google.inject.Injector;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Gumo processor.
 *
 * Main class for applying methods to a data type instance.
 */
public class Processor {

    private final GumoModule module;
    private final Injector injector;

    /**
     * Create a new processor.
     *
     * @param module the processor configuration.
     * @param injector a Guice injector.
     */
    public Processor(final GumoModule module, final Injector injector) {
        this.module = module;
        this.injector = injector;
    }

    /**
     * Apply processor configuration to the specified input.
     *
     * @param in input data.
     * @param names names of bindings to apply (default without name is always applied).
     * @param <IN> data input type.
     * @param <OUT> result data type.
     * @return a possible return value or null.
     */
    public <IN, OUT> OUT apply(final IN in, final String... names) {
        return apply(in, null, names);
    }

    private <IN, OUT> OUT apply(final IN in, final OUT defaultValue, final String...names) {
        final List<GumoBinding> bindings = module.getBindings(in.getClass(), names);
        if (bindings.size() > 0) {
            // First apply all consumers.
            bindings.stream().filter(b -> b.getDestinationConsumerType() != null).forEach( binding -> {
                //noinspection unchecked
                injector.getInstance((Class<? extends Consumer<IN>>) binding.getDestinationConsumerType()).accept(in);
            });
            // Next apply the first function and apply the result value to this processor possibly resulting in more processing.
            //noinspection unchecked
            return bindings.stream()
                    .filter(b -> b.getDestinationFunctionType() != null)
                    .map(b -> {
                        //noinspection unchecked
                        final Object result = injector.getInstance((Class<? extends Function<IN, Object>>) b.getDestinationFunctionType()).apply(in);
                        //noinspection unchecked
                        return (OUT) apply(result, result, names);
                    })
                    .findFirst().orElse(defaultValue);
        }
        return defaultValue;
    }
}
